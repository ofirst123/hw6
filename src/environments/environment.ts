// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB1oSsQm26cIYuqe5uS67wXzgJACUCEpzQ",
    authDomain: "hw06-84d71.firebaseapp.com",
    databaseURL: "https://hw06-84d71.firebaseio.com",
    projectId: "hw06-84d71",
    storageBucket: "hw06-84d71.appspot.com",
    messagingSenderId: "153673282634"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
